import React, { Component } from 'react'
import { connect } from "react-redux";
import PropTypes from "prop-types";
import {getAllBlocks} from "../../Redux/action/blockAction";
import ShowAllBlock from './ShowAllBlock';
import Pagination from '../Paginator/Pagination';

 class AllBlock extends Component {
  //Initializing State
  state = 
  { allBlock: [], currentShowBlock: [], currentPage: null, totalPages: null }
  
  async componentDidMount() {
     
    this.props.getAllBlocks();
    
  }

  onPageChanged = data => {
      
    const { allBlock } = this.state;
    const { currentPage, totalPages, pageLimit } = data;
    const offset = (currentPage - 1) * pageLimit;
    const currentShowBlock = allBlock.slice(offset, offset + pageLimit);
    this.setState({ currentPage, currentShowBlock, totalPages });
    }
    
  componentWillReceiveProps(nextProps) {
      
    this.setState({ allBlock:nextProps.allBlocks});
  }

  render() {
    
    const { allBlock, currentShowBlock, currentPage, totalPages } = this.state;
    const totalBlock = allBlock.length;
    
    if (totalBlock === 0) return null;
    const headerClass = ['text-dark py-2 pr-4 m-0', currentPage ? 'border-gray border-right' : ''].join(' ').trim();

    return (

      <React.Fragment>

        <div className="col-md-12">
          <div className="row text-left">
              <div className="col-md-6 col-sm-6 col-lg-6 col-xs-12 no-padding">
                  <h1>All Blocks</h1>
                  <p className="heading-sm">{this.props.totalBlockCount} Blocks Found</p>
                  <p className="dashboard-p">{this.props.totalBlockCount>=20 ? "Showing Latest "+totalBlock+ " Blocks":"Showing all "+this.props.totalBlockCount+ " blocks"}</p> 
              </div>
              <div className="col-md-6 col-sm-6 col-lg-6 col-xs-12">
              </div>
          </div>
        </div>
    
        <div>
          <div className="row">
            <div className="col-md-12 col-lg-12 col-sm-12 col-xs-12  m-t-10 m0-p0">
              <div className="table-bg">
                <div className="clear"></div>
                  <div className="table-responsive">
                    <table className="table table-striped">
                      <thead>
                        <tr>
                          <th>Block</th>
                          <th>Age</th>
                          <th>Txn</th>
                          <th>Miner</th>
                          <th>GasUsed</th>
                          <th>GasLimit</th>
                          <th>Reward</th>
                        </tr>
                      </thead>
                      <tbody>
                          {
                            currentShowBlock.map((block,index) => (
                            <ShowAllBlock key={index} block={block} />))
                          }
                      </tbody>
                    </table>
                  </div>       
                </div>
              </div>
            </div>
          <div className="row text-center">
                <nav className="pagination-t">
                  <Pagination totalRecords={totalBlock} pageLimit={10} pageNeighbours={1} onPageChanged={this.onPageChanged} />
                </nav>
          </div>
        </div>

      </React.Fragment>
    );
  }
}
  
  // proptype declaration
  AllBlock.propTypes = {
    getAllBlocks : PropTypes.func.isRequired,
  };
  
  //get allblocks from the store
  const mapStateToProps = state => ({
      allBlocks: state.blocksStore.allBlocks,
      totalBlockCount : state.blocksStore.totalBlockCount
  });
  
  //connect component to redux
  export default connect(mapStateToProps,{getAllBlocks})(AllBlock);
  