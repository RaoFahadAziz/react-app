import React, { Component } from "react";
import { getSingleBlockDetail } from "../../Redux/action/blockAction";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import ShowSingleBlock from "./ShowSingleBlock";
import ErrorNotFound from '../NotFound/ErrorNotFound';

class SingleBlock extends Component {
  constructor(props) {
    super(props)
    this.state = {
      data: Number
    };

    /*
     Bind our childHandler function to this context
     that will get called from our Child component
    */
    this.childHandler = this.childHandler.bind(this)
  }

  async childHandler(dataFromChild) {
   
    await this.setState({
      data: dataFromChild
    });
    
    this.props.getSingleBlockDetail(this.state.data);
  }

  async componentDidUpdate(prevProps) {
    if (this.props.match.params.blockNumber !== prevProps.match.params.blockNumber) {
      this.props.getSingleBlockDetail(this.props.match.params.blockNumber);
    
    }
  }

  componentDidMount() {
    
    this.props.getSingleBlockDetail(this.props.match.params.blockNumber); //get block detail
  }

  render() {
    const { singleBlock } = this.props;
    console.log("single block", this.props.singleBlock.response);

    return (
      
      this.props.singleBlock.response === "Block not exist"?<ErrorNotFound data = {"block"+" "+this.props.match.params.blockNumber}/>:
      <React.Fragment>
        <div className="col-md-12">
          <div className="row text-left">
            <div className="col-md-6 col-sm-6 col-lg-6 col-xs-12" />
          </div>
          
          <div className="row m-t-30">
            <p className="dashboard-p" style={{marginTop:20,fontSize:20}}><b style={{color:'white'}}>Block Number</b>#{this.props.singleBlock.Height}</p>
          </div>
        
        </div>
        <ShowSingleBlock singleBlock={singleBlock} action={this.childHandler} blockNumber ={this.props.match.params.blockNumber} />
      </React.Fragment>
     
    );
  }
}

// proptype declaration
SingleBlock.propTypes = {
  getSingleBlockDetail: PropTypes.func.isRequired
};

//get blockDetail from the store
const mapStateToProps = state => ({
  singleBlock: state.blocksStore.singleBlock
});
//connect component to redux
export default connect(mapStateToProps,{ getSingleBlockDetail })(SingleBlock);
