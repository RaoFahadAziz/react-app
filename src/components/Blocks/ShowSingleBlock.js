import React, { Component } from "react";
import { Link,withRouter } from "react-router-dom";
import timeDiff from "../../Helpers/TimeDifference";
import PropTypes from "prop-types";

class ShowSingleBlock extends Component {
  constructor(props) {
    super(props);
    this.onNextBlock = this.onNextBlock.bind(this);
    this.onPreviousBlock = this.onPreviousBlock.bind(this);
  }

  onNextBlock(e){
    e.preventDefault();
    let blockNumber = parseInt(this.props.blockNumber,10);
    let nextBlockNumber = blockNumber + 1;
    this.props.action(nextBlockNumber);
    this.props.history.push('/block/'+nextBlockNumber);
  }

  onPreviousBlock(e){
    e.preventDefault();
    let blockNumber = parseInt(this.props.blockNumber,10);
    let nextBlockNumber = blockNumber - 1;
    this.props.action(nextBlockNumber);
    this.props.history.push('/block/'+nextBlockNumber);
  }
  render() {
    
    // Destructure props for readability
    const {
      Height,
      TimeStamp,
      NoOfTransaction,
      BlockReward,
      Difficulty,
      TotalDifficulty,
      transactionFee,
      GasUsed,
      GasLimit,
      extraData,
      Hash,
      ParentHash,
      Nonce,
      Size,
      Miner,
     } = this.props.singleBlock;
    const blockTimestamp = timeDiff(TimeStamp);

    return (
    <div className="row">
      <div className="col-md-12 col-lg-12 col-sm-12 col-xs-12  m-t-10 m0-p0">
        <div className="tabs-bg1">
          <ul className="nav nav-tabs">
            <li className="active btn-tab">
              <a data-toggle="tab" href="#home">
                Overview
              </a>
            </li>
          </ul>
          <div className="col-md-12 col-lg-12 col-sm-12 col-xs-12 bg-trans">
            <div className="col-md-12 col-lg-12 col-sm-12 col-xs-12 mob-transaction-text">
              <p className="font-tab-pg3 dis-inline-block">
                Block information
              </p>
              <span className="dis-inline-block m-l-8 m-r-8 angle-pg3" onClick={this.onPreviousBlock} >
                <img className="angles-img" src="/img/angle-l.png"  />
              </span>
              <span className=" angle-pg3 dis-inline-block " onClick={this.onNextBlock}>
                <img className="angles-img" src="/img/angle-r.png" />
              </span>
            </div>
          </div>

          <div className="tab-content">
            <div id="home" className="tab-pane fade in active">
              <div className="table-bg">
                <div className="clear" />
                <div className="table-responsive ver-table-pg3">
                  <table className="table-line-height" style={{ width: "100%" }}>
                    <tbody>
                      <tr>
                        <th className="col-xs-4"> BLock Height:</th>
                        <td className="">{Height}</td>
                      </tr>
                      
                      <tr>
                        <th className="col-xs-4"> TimeStamp:</th>
                        <td className="">{blockTimestamp}</td>
                      </tr>

                      <tr>
                        <th className="col-xs-4">Transactions:</th>
                        <td className=""><Link to={`/tx/${Height}`}>{NoOfTransaction}</Link> Transactions</td>
                      </tr>

                      <tr>
                        <th className="col-xs-4">Mined By:</th>
                        <td className=" text-truncate">{Miner}</td>
                      </tr>
                      
                      <tr>
                        <th className="col-xs-4">Block Reward:</th>
                        <td className=""> {BlockReward} SGC (2+{transactionFee})</td>
                      </tr>
                    
                      <tr>
                        <th className="col-xs-4">Difficulty:</th>
                        <td className="">{Difficulty}</td>
                      </tr>
                      
                      <tr>
                        <th className="col-xs-4">Total Difficulty:</th>
                        <td className="">{TotalDifficulty}</td>
                      </tr>

                      <tr>
                        <th className="col-xs-4">Size:</th>
                        <td className="">{Size} bytes</td>
                      </tr>

                      <tr>
                        <th className="col-xs-4">Gas Used:</th>
                        <td className="">{GasUsed}</td>
                      </tr>

                      <tr>
                        <th className="col-xs-4">Gas Limit:</th>
                        <td className=""> {GasLimit}    </td>
                      </tr>

                      <tr>
                        <th className="col-xs-4">Extra Data:</th>
                        <td className=""> (Hex:{extraData}) </td>
                      </tr>

                      <tr>
                        <th className="col-xs-4">Hash:</th>
                        <td className=""> {Hash} </td>
                      </tr>
                      
                      <tr>
                        <th className="col-xs-4">Parent Hash:</th>
                        <td className=""><Link to ={`/blocks/${ParentHash}`}>{ParentHash}</Link> </td>
                      </tr>
                      
                      <tr>
                        <th className="col-xs-4">Nonce:</th>
                        <td className=""> {Nonce}</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
   );
 }
}
 // proptype declaration
ShowSingleBlock.propTypes = {
    singleBlock: PropTypes.object.isRequired
};

export default withRouter(ShowSingleBlock);
