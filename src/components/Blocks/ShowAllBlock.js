import React, { Component } from 'react'
import { Link } from "react-router-dom";
import timeDiff from '../../Helpers/TimeDifference'
import PropTypes from 'prop-types'
import stringTrunc from "../../Helpers/Truncate";


class ShowAllBlock extends Component {
  render() {
    // Destructure props for readability
    const {Height,TimeStamp,Miner,GasUsed,GasLimit,NoOfTransaction,BlockReward,noOfUncle} = this.props.block;
    const blockTime = timeDiff(TimeStamp)
    const minerHash = stringTrunc(Miner);
    
    return (
    
      <tr>
        <td><Link to={`/block/${Height}`}>{Height}</Link></td>
        <td>{blockTime}</td>
        <td><Link to={`/tx/${Height}`}>{NoOfTransaction}</Link></td>
        <td>{minerHash}</td>
        <td>{GasUsed}</td>
        <td>{GasLimit}</td>
        <td>{BlockReward} Ether</td>
      </tr>
   )
  }
}
// proptype declaration
ShowAllBlock.propTypes = {
  block : PropTypes.object.isRequired,
};

export default ShowAllBlock;