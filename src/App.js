import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Blocks from './components/Home/Blocks';
import SingleBlock from './components/Blocks/SingleBlock';
import AllBlock from './components/Blocks/AllBlock';
import BlockTx from './components/Transactions/BlockTx';
import SingleTx from './components/Transactions/SingleTx';
import AllTx from './components/Transactions/AllTx';
import Header from './components/layout/Header';
import Footer from './components/layout/Footer';
import ErrorNotFound from './components/NotFound/ErrorNotFound';
import SingleAddDetail from './components/Account/SingleAddDetail';
import AllUncle from './components/Uncle/AllUncle';
import SingleUncle from './components/Uncle/SingleUncle';
import Certificate from './components/Certificate/Certificate'
import {Provider} from 'react-redux';
import store from './store';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';



class App extends Component {
  render() {
    return (
   

  <Provider store = {store}>

    <Router>
      <div className="App">
        <div className=" margin-0 padding-0">
          <div className="main-bg">
            <Header/>
              <div className="container m-t-20per m-b-100">
                <Switch>
                  <Route exact path="/" component={Blocks} />
                  <Route exact path="/blocks" component={AllBlock} />
                  <Route exact path = "/tx/:blockNumber" component = {BlockTx} />
                  <Route exact path = "/txs/:hash" component = {SingleTx} />
                  <Route exact path = "/block/:blockNumber" component = {SingleBlock} />
                  <Route exact path = "/allTx" component = {AllTx} />
                  <Route exact path = "/address/:address" component = {SingleAddDetail} />
                  <Route exact path = "/uncles" component = {AllUncle} />
                  <Route exact path = "/uncles/:hash" component = {SingleUncle} />
                  <Route exact path = "/certificate" component = {Certificate} />
                  <Route exact path = "/notFound" component = {ErrorNotFound} />
                  <Route component={ErrorNotFound} />
                </Switch> 
              </div>
          </div>
        </div>
        <Footer/>
      </div>
    </Router>
  
  </Provider>

    );
  }
}

export default App;
