import {CURRENT_CHAIN_DIFF,CURRENT_BLOCK_DIFF,GET_ALL_BLOCK,GET_ALL_BLOCK_COUNT,GET_LATEST_BLOCK_DETAIL,CURRENT_BLOCK,GET_SINGLE_BLOCK_DETAIL,GET_SINGLE_BLOCK_DETAIL_Hx} from "../action/type";

const initialState = {
  
  latestBlocks: [],
  allBlocks:[],
  singleBlock : {},
  allTransactionInOneBlock : [],
  current_Block : '',
  currentBlockDiff : '',
  chainDiff : '',
  singleBlockHx : {},
  totalBlockCount:'String'
  
};

export default function(state = initialState, action) {
  switch (action.type) {

    case GET_LATEST_BLOCK_DETAIL:
      return {
        ...state,
        latestBlocks: action.payload//populate array with All blockinformation object
      };

    case GET_SINGLE_BLOCK_DETAIL:
      return {
        ...state,
        singleBlock: action.payload//blockinformation object
      };

    case GET_ALL_BLOCK:
      return {
        ...state,
        allBlocks: action.payload,//populate array with All blockinformation object
       
      };
      case GET_ALL_BLOCK_COUNT:
      return {
        ...state,
        totalBlockCount : action.payload //total number of block
      };

    case CURRENT_BLOCK:
  
      return {
        ...state,
        current_Block: action.payload.currentBlock   //current block record
      };
      
    case CURRENT_BLOCK_DIFF:
      
        return {
          ...state,
          currentBlockDiff: action.payload.currentBlockDiff 
        };

    case CURRENT_CHAIN_DIFF:
  
        return {
          ...state,
          chainDiff: action.payload.chainDiff   //Chain diff
};
















    default:
      return state;
  }
}
