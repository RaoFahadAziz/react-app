import {
  CURRENT_BLOCK,
  GET_ALL_BLOCK,
  GET_LATEST_BLOCK_DETAIL,
  GET_SINGLE_BLOCK_DETAIL,
  GET_SINGLE_BLOCK_DETAIL_Hx,
  GET_ALL_BLOCK_COUNT,
  CURRENT_CHAIN_DIFF,
  CURRENT_BLOCK_DIFF
} from "./type";

import axios from "axios";
import baseUrl from "../../Helpers/baseUrl";

// Get First 10  blockDetail from the database
export const getLatestBlocks = () => async dispatch => {
  const res = await axios.get(`${baseUrl}:5000/api/latestBlock`);

  try {
    dispatch({
      type: GET_LATEST_BLOCK_DETAIL,
      payload: res.data
    });
  } catch (e) {
    dispatch({
      type: GET_LATEST_BLOCK_DETAIL,
      payload: null
    });
  }
};

// Get Single block Detail from Database
export const getSingleBlockDetail = blockNumber => async dispatch => {
  const blockNumbers = {
    block: blockNumber
  };
  const res = await axios.post(`${baseUrl}:5000/api/singleBlockDetail`,blockNumbers
  );

  try {
    dispatch({
      type: GET_SINGLE_BLOCK_DETAIL,
      payload: res.data
    });
  } catch (e) {
    dispatch({
      type: GET_SINGLE_BLOCK_DETAIL,
      payload: null
    });
  }
};

//get All Block
export const getAllBlocks = () => async dispatch => {
  const res = await axios.get(`${baseUrl}:5000/api/allBlock`);
  
  try {
    dispatch({
      type: GET_ALL_BLOCK,
      payload: res.data.result
    });
    dispatch({
      type: GET_ALL_BLOCK_COUNT,
      payload: res.data.totalBlock
    });
  } catch (e) {
    dispatch({
      type: GET_ALL_BLOCK,
      payload: null
    });
  }
};

// get Current block number
export const currentBlockInfo = () => async dispatch => {
  const res = await axios.get(`${baseUrl}:5000/api/currentBlock`);
  
  try {
    dispatch({
      type: CURRENT_BLOCK,
      payload: {currentBlock:res.data.blockNumber}
    });
    dispatch({
      type: CURRENT_BLOCK_DIFF,
      payload: {currentBlockDiff:res.data.currentBlockDiff}
    });

    dispatch({
      type: CURRENT_CHAIN_DIFF,
      payload: {chainDiff:res.data.chainDiff}
    });
  } catch (e) {
    dispatch({
      type: CURRENT_BLOCK,
      payload: null
    });
  }
};
