const timeDiff =  (timeStamp)=>{
    
    var unix = Math.round(+new Date() / 1000);
    var difference = Math.abs(new Date(unix * 1000) - new Date(timeStamp * 1000)) / 1000;
    var days = Math.floor(difference / 86400);
    // get hours        
    var hours = Math.floor(difference / 3600) % 24;        
    // get minutes
    var minutes = Math.floor(difference / 60) % 60;
    // get seconds
    var seconds = res % 60;
    var formattedTime =days + "days" + " " +hours +"h" +" " +minutes +"m" +" " +seconds+"s";
    return formattedTime;
  }
export default timeDiff;
  