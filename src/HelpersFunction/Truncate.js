const stringTrunc =  (longString)=>{
    if (longString.length > 16)
       return longString.substring(0,15)+'...';
    else
       return longString;
  };
export default stringTrunc;