var AmCharts = AmCharts;

var chart1 = AmCharts.makeChart("chartdiv", {
    "type": "serial",
    "theme": "light",
    "dataProvider": [{

        "date": "2009-10-02",
        "value": 25
    }, {
        "date": "2009-10-03",
        "value": 15
    }, {
        "date": "2009-10-04",
        "value": 13
    }, {
        "date": "2009-10-05",
        "value": 17
    }, {
        "date": "2009-10-06",
        "value": 15
    }, {
        "date": "2009-10-09",
        "value": 19
    }, {
        "date": "2009-10-10",
        "value": 21
    }, {
        "date": "2009-10-11",
        "value": 20
    }, {
        "date": "2009-10-12",
        "value": 20
    }, {
        "date": "2009-10-13",
        "value": 19
    }, {
        "date": "2009-10-16",
        "value": 25
    }, {
        "date": "2009-10-17",
        "value": 24
    }, {
        "date": "2009-10-18",
        "value": 26
    }, {
        "date": "2009-10-19",
        "value": 27
    }, {
        "date": "2009-10-20",
        "value": 25
    }, {
        "date": "2009-10-23",
        "value": 29
    }, {
        "date": "2009-10-24",
        "value": 28
    }, {
        "date": "2009-10-25",
        "value": 30
    }, {
        "date": "2009-10-26",
        "value": 72,
        "customBullet": "http://localhost/bes-explorer/img/chart-pointer.png"
    }, {
        "date": "2009-10-27",
        "value": 43
    }, {
        "date": "2009-10-30",
        "value": 31
    }, {
        "date": "2009-11-01",
        "value": 30
    }, {
        "date": "2009-11-02",
        "value": 29
    }, {
        "date": "2009-11-03",
        "value": 27
    }, {
        "date": "2009-11-04",
        "value": 26
    }],
    "valueAxes": [{
        "axisAlpha": 0,
        "dashLength": 4,
        "position": "left"
    }],
    "graphs": [{
        "bulletSize": 14,
        "lineColor": "#D71920",
        "lineThickness": 2,
        "customBullet": "",
        "customBulletField": "customBullet",
        "valueField": "value",
        "balloonText": "<div style='margin:10px; text-align:left;'><span style='font-size:13px'>[[category]]</span><br><span style='font-size:18px'>Value:[[value]]</span>",
    }],
    "marginTop": 20,
    "marginRight": 10,
    "marginLeft": 40,
    "marginBottom": 20,
    "chartCursor": {
        "graphBulletSize": 1.5,
        "zoomable": false,
        "valueZoomable": true,
        "cursorAlpha": 0,
        "valueLineEnabled": true,
        "valueLineBalloonEnabled": true,
        "valueLineAlpha": 0.2
    },
    "autoMargins": false,
    "dataDateFormat": "YYYY-MM-DD",
    "categoryField": "date",
    "valueScrollbar": {
        "offset": 30
    },
    "categoryAxis": {
        "parseDates": true,
        "axisAlpha": 0,
        "gridAlpha": 0,
        "inside": true,
        "tickLength": 0
    },
    "export": {
        "enabled": true
    }
});


var chart2 = AmCharts.makeChart("pieChart", {
    "type": "pie",
    "theme": "light",
    "colors": ['#000000', '#8D8D8D', '#6D6D6D', '#606060', '#D9D9D9', 'rgba(175,175,175,0.82)'],
    "dataProvider": [{
        "title": "New",
        "value": 4852
    }, {
        "title": "Returning",
        "value": 9899
    }, {
        "title": "New",
        "value": 4852
    }, {
        "title": "Returning",
        "value": 9899
    }, {
        "title": "New",
        "value": 4852
    }, {
        "title": "Returning",
        "value": 9899
    }],
    "titleField": "title",
    "valueField": "value",
    "labelRadius": 5,

    "radius": "42%",
    "innerRadius": "60%",
    "labelText": "[[title]]",
    "export": {
        "enabled": true
    }
});